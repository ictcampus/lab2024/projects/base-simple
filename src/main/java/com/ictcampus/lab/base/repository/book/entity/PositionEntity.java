package com.ictcampus.lab.base.repository.book.entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */
@Data
@Entity
@Table( name = "positions" )
public class PositionEntity {
	@Id
	private Long id;

	private String floor;
	private String sector;
	private String rack;
	private String line;

	@OneToMany(mappedBy = "position", cascade = CascadeType.ALL, fetch = FetchType.LAZY) @ToString.Exclude
	List<BookEntity> books = new ArrayList<>();
}
