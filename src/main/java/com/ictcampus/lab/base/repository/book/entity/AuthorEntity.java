package com.ictcampus.lab.base.repository.book.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.ToString;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */
@Data
@Entity
@Table( name = "authors" )
public class AuthorEntity {
	@Id
	private Long id;

	private String name;
	private String surname;
	private String nickname;
	private LocalDate birthday;

	@ManyToMany(mappedBy = "authors") @ToString.Exclude
	List<BookEntity> books = new ArrayList<>();
}
