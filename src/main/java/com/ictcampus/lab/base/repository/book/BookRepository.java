package com.ictcampus.lab.base.repository.book;

import com.ictcampus.lab.base.repository.book.entity.BookEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */
@Repository
public interface BookRepository extends JpaRepository<BookEntity, Long>, BookRepositoryCustom {
	BookEntity findByTitle(String title);
}
