package com.ictcampus.lab.base.service.book;

import com.ictcampus.lab.base.repository.book.BookRepository;
import com.ictcampus.lab.base.repository.book.entity.BookEntity;
import com.ictcampus.lab.base.service.book.mapper.BookServiceStructMapper;
import com.ictcampus.lab.base.service.book.model.Book;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */
@Service
@AllArgsConstructor
@Slf4j
public class BookService {
	@Autowired
	private BookRepository bookRepository;

	@Autowired
	private BookServiceStructMapper bookServiceStructMapper;

	public List<Book> getBooks() {
		List<BookEntity> bookEntities = bookRepository.findAll();
		return bookServiceStructMapper.toBooks(bookEntities);
	}

	public List<Book> getBooksCustom() {
		List<BookEntity> bookEntities = bookRepository.findBooksWithExtendData();
		return bookServiceStructMapper.toBooks(bookEntities);
	}

	public Book getBookById(Long id) {
		BookEntity bookEntity = bookRepository.findById(id).orElse(null);
		return bookServiceStructMapper.toBook(bookEntity);
	}

	public Book getBookByTitle(String title) {
		BookEntity bookEntity = bookRepository.findByTitle(title);
		return bookServiceStructMapper.toBook(bookEntity);
	}

}
