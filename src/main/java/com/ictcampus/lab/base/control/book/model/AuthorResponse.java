package com.ictcampus.lab.base.control.book.model;

import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

import java.time.LocalDate;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */
@Value
@Builder
@Jacksonized
public class AuthorResponse {
	Long id;
	String name;
	String surname;
	String nickname;
	LocalDate birthday;
}
